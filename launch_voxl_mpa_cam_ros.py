import os
import socket
import fcntl
import struct

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

ip_addr=get_ip_address()
ros_master_uri='http://'+ip_addr+':11311/'

os.environ["ROS_IP"]=ip_addr
os.environ["ROS_MASTER_URI"]=ros_master_uri

os.system('roslaunch /opt/ros/indigo/share/voxl_mpa_cam_ros/launch/voxl_mpa_cam_ros.launch')